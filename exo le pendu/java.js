
String.prototype.replaceAt=function(index, character) 
{ return this.substr(0, index) + character + this.substr(index+character.length); }



const palabras = ['philanthropies', 'divergeassions', 'décomplétaient', 'pneumatologies', 'investissement', 'rapportassions', 'commensalismes', 'dépoussiérerez', 'coquetteraient', 'réhydratassiez' ,'impressionnées' ,'consolidassiez']

const palabra = palabras [Math.floor (Math.random()*palabras.length)]
let contadorFallos = 0;

let palabraConGuiones = palabra.replace(/./g, "_ ");

document.querySelector('#output').innerHTML = palabraConGuiones;
document.querySelector('#calcular').addEventListener('click', () => {
    
    const letra = document.querySelector('#mot').value
    let hafallado = true
    for(const i in palabra){
        if (letra == palabra[i]){
            palabraConGuiones = palabraConGuiones.replaceAt(i*2,letra)
            hafallado = false
        }
    }

    if (hafallado){
        contadorFallos++;
        document.querySelector('#pendu').style.backgroundPosition = - (142*contadorFallos) +'px';
        if(contadorFallos==5){
            document.querySelector('#perdu').style.display ='flex';}
    } else{
        if (palabraConGuiones.indexOf('_')<0){
            document.querySelector('#gagne').style.display ='flex';
            }
    }

    document.querySelector('#output').innerHTML = palabraConGuiones;


    document.querySelector('#mot').value = '';
    document.querySelector('#mot').focus();
})
